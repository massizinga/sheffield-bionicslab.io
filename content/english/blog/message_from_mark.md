---
title: "A Message From Mark"
date: 2020-07-16T09:07:04+01:00
author: Thomas Binu Thomas
image: images/blog/gofundme.jpg
description: "An update from Mark on our crowdfunding campaign"
---

# A Message from Mark

The first 11 days of crowdfunding has been surprisingly great! We have raised £804 and have ran 136km for the cause! Here's a look at what Mark Y-ted Whitehead had to say about this.
GoFundMe Link: https://gf.me/u/ydxnna

{{< youtube DmxTlCJluRk >}}
