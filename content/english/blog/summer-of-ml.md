---
title: "Summer of ML (Part 1)"
date: 2020-09-20T08:12:56+01:00
author: Brooks Rady
image: images/blog/summer-of-ml/banner.png
description: "A high-level overview of what the arm ML team has accomplished this summer"
---

This is the first of three blog posts exploring the work of the Arm project's machine learning sub-team. The team is composed of Thomas Thomas and me, and this post will give a high-level overview of what we've accomplished. The second post will be a technical deep-dive into how we developed our machine learning model, and the third will shine some light on our current and future endeavors.

## The Big Picture

In building a custom bionic arm for our amputee, Mark, it's not only important that the prosthetic comfortably fits his residual limb, but also that he's provided with an intuitive way to control it; that's where we come in.

In pursuit of this intuitive control, we set out to develop an electromyography (EMG) based control system. This sort of system uses a special arm-band to detect muscular activity within the residual limb, then decodes the raw electrical signals into a set of predefined "gestures". Over the summer, we've taken to developing a gesture classification model to do just that. In the final arm, this model will be run by a small onboard computer to decode Mark's muscle movements in real-time.

## Starting Fresh

While that sounded like a great plan, the reality was, we had no idea where to start. After a little research, however, we identified this as the perfect problem for machine learning. With that in mind, we settled on using our [Myo armband](https://support.getmyo.com/hc/en-us) to non-invasively record EMG data from the pilot and [Tensorflow](https://www.tensorflow.org/) to build our machine learning model.

While both Thomas and I had existing Python experience, Tensorflow and machine learning were new to us. To learn more, we turned to a number of free resources. First was [Google's Machine Learning Crash Course](https://developers.google.com/machine-learning/crash-course/). This provided us with much of the prerequisite understanding we needed to get started and made it clear that we had a classification problem on our hands. Following on from that was [this tutorial](https://machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras/) showing how Keras (now a part of Tensorflow 2.0) could be used to classify sequential data – sequential data like our readings from the Myo armband. Finally, that tutorial introduced us to Long-Short Term Memory networks (LSTMs) and Convolutional Neural Networks (CNNs) which we learned more about [here](http://colah.github.io/posts/2015-08-Understanding-LSTMs/) and [here](https://cs231n.github.io/convolutional-networks/) respectively.

With a clear goal in mind and these jumping-off points, all that was left to do was get started! From the 21st of July through the 8th of September, Thomas and I met every week for a 6-8 hour coding sprint on the project.

## Data Collection

Before we could teach our model what different gestures looked like, we needed some data to serve as an example. Early in the summer we met with our amputee, Mark, to collect some EMG readings. We had Mark perform a number of gestures using his residual limb and saved the data from the Myo for later processing.

Unfortunately, this first attempt was rather naive, and even our best machine-learning model could only classify ~65% of the gestures correctly. The data were muddled by several factors, but our biggest issue was that we just needed more of it. Realising that we would likely expose many more shortcomings before our model was developed, we elected to refine our process using data collected from within the ML team.

This second pass at data collection benefited from our experience with Mark and allowed us to collect a much larger set of data. This data went on to yield *much* better results during the training process. Perhaps most importantly, we came away with a better understanding of what we needed from Mark going forwards.

## Results

After a bit of tweaking and a lot of reading up on various neural networks, we ended up with some rather impressive results: 98.3% ± 1.92% of the data collected from Thomas was correctly classified.

Naturally, the next step was attempting the same classification but in live time! We rigged up some Python code for reading data from the Myo and classifying it simultaneously, then had Thomas try it out. This was our result:

{{< youtube XzA5VC78EiI >}}

Okay! That's not half bad! We could reliably get it to recognize 3-4 of the 8 gestures we trained it on. Okay, so maybe that's exactly half bad, but it's quite promising nonetheless!

## Further Improvement

So what went wrong? Well, the working theory is a lack of diversity in our training data. All of it was recorded in a single take without re-adjusting the Myo or performing the gestures in different positions. As a result, it's possible that our model came to rely on a very particular sensor positioning or some other confounding factor.

There are a couple of potential solutions to this problem, but the most effective is likely to be the collection of more varied data. Unfortunately, the current data-collection system makes this difficult. The interface provides no feedback as to how many repeats of a gesture have been gathered and doesn't allow us to go back and add repeats to existing gestures – making recording from several people, or in different positionings, quite impractical.

To solve this, we're currently in the process of designing a more user-friendly data collection system that provides live, visual feedback to the technician and enables out-of-order repeat collection. The plans for this application and our other on-going efforts we'll elaborate on in part 3 of this series. But before that, it's time for a technical deep-dive!

See you in part 2!