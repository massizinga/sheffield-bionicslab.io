---
title: "Spring Intro Talk"
date: 2021-02-19T16:18:32Z
author: Alex Pop
image: images/blog/welcome2021.png
description: "Project leaders show off what they will be doing this term"
---

Hi everyone, we hope you’ve had a great Christmas period, that exams went well and that you have had a chance to rest after all of it. With the new spring term we are also starting again here at Sheffield Bionics - this time with University funding!

To give project leaders a chance to show off what they will be doing this term we’ve just had our Spring Intro Talk, though dont worry if you missed it, here are some of the key points:

- **Arm**: Will finish the newest prototype and give it to Mark for testing
- **Leg**: Carrying on the great work from last term and making their first prototype
- **BCI**: Working with the brand new OpenBCI EEG Headset they have bought
- **Bootcamp**: Deepening skills gained in 1st term with advanced sessions

If you are keen to see the slides used, here is [a link to the presentation](https://docs.google.com/presentation/d/11UfDdEf7iWFCLTpecrZeXepJ81z6880tHRk55IpG8zM/edit)

If you are a new member or are interested in joining please contact us or come along to one of our meetings, no previous experience required, just an eagerness to learn.

If you are an existing member or you are keen to make your mark on the society why not also come along to our committee meetings every Thursday at 5pm. Here you can see how the inner gears of the society work, what we spend our time and money on, all of which will help you become ready for being a committee member next year.

You can [join using this link](https://meet.google.com/jii-trji-mmc)
